# Misc
.DEFAULT_GOAL = help
.PHONY        : help docker git


## —— Commands list 🛠️️ —————————————————————————————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'


## —— Grumphp 😡 ———————————————————————————————————————————————————————————————
grum: ## Run grumphp
	php ./vendor/bin/grumphp run


## —— Git 🔀 ———————————————————————————————————————————————————————————————————
reset: ## Reset last commit on local
	git reset --soft HEAD^


## —— Tests ✅ —————————————————————————————————————————————————————————————————
unit: ## Run unit tests only, with phpunit
	vendor/bin/phpunit tests --testdox
