<?php

namespace PhilallSimpleCaptcha\Tests\unit;

use GdImage;
use PHPUnit\Framework\TestCase;
use PhilallSimpleCaptcha\Picture\GdHandler;
use PhilallSimpleCaptcha\Tests\ProviderTrait;

class GdHandlerTest extends TestCase
{
    use ProviderTrait;

    private GdHandler $gdHandler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->gdHandler = new GdHandler();
    }

    /**
     * @test
     * @dataProvider contrastProvider
     */
    public function testGetGdImage(?string $contrast): void
    {
        $sentence = "a sentence";
        $fontSize = 12;

        $gdObject = $this->gdHandler->getGdImage($sentence, $fontSize, $contrast);

        $this->assertInstanceOf(GdImage::class, $gdObject);
    }
}
