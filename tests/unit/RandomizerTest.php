<?php

namespace PhilallSimpleCaptcha\Tests\unit;

use PHPUnit\Framework\TestCase;
use PhilallSimpleCaptcha\Tests\ProviderTrait;
use PhilallSimpleCaptcha\DataProvider\Randomizer;

class RandomizerTest extends TestCase
{
    use ProviderTrait;

    private Randomizer $randomizer;

    protected function setUp(): void
    {
        parent::setUp();
        $this->randomizer = new Randomizer();
    }

    /**
     * @test
     * @dataProvider charsetProvider
     */
    public function testGetSentence(string $charsetName, string $negativeRegex): void
    {
        $length   = rand(5, 10);
        $sentence = $this->randomizer->getSentence($charsetName, $length);

        $this->assertInstanceOf(Randomizer::class, $this->randomizer);
        $this->assertEquals(true, !preg_match($negativeRegex, $sentence));
        $this->assertEquals($length, strlen($sentence));
    }
}
