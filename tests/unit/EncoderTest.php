<?php

namespace PhilallSimpleCaptcha\Tests\unit;

use PHPUnit\Framework\TestCase;
use PhilallSimpleCaptcha\DataTransformer\Encoder;

class EncoderTest extends TestCase
{
    private Encoder $encoder;

    protected function setUp(): void
    {
        parent::setUp();
        $this->encoder = new Encoder();
    }

    /** @test */
    public function testGetEncrypt(): void
    {
        $sentence = 'test sentence';
        $encrypt  = $this->encoder->getEncrypt($sentence);

        $this->assertInstanceOf(Encoder::class, $this->encoder);
        $this->assertIsString($encrypt);
        $this->assertNotEquals($encrypt, $sentence);
    }
}
