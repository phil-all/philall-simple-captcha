<?php

namespace PhilallSimpleCaptcha\Tests;

use Generator;

trait ProviderTrait
{
    /**
     * Provide a charset name and its negative regular expression
     */
    public function charsetProvider(): Generator
    {
        yield ['numeric', '/[^\d]/'];
        yield ['alphabetic', '/[^a-zA-Z]/'];
        yield ['lowercase', '/[^a-z]/'];
        yield ['uppercase', '/[^A-Z]/'];
        yield ['alphanumeric', '/[^a-zA-Z\d]/'];
    }

    /**
     * Provide a contrast
     */
    public function contrastProvider(): Generator
    {
        yield ['dark'];
        yield [null];
    }
}
