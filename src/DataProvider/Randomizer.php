<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\DataProvider;

class Randomizer implements RandomizerInterface
{
    private const DIGIT_CHARSET = '123456789';

    private const LOWER_CHARSET = 'abcdefghijklmnpqrstuvwxyz';

    private const UPPER_CHARSET = 'ABCDEFGHIJKLMNPQRSTUVWXYZ';

    /**
     * @inheritDoc
     */
    public function getSentence(string $charsetName, int $length): string
    {
        return $this->generateSentence($this->getCharsetContent($charsetName), $length);
    }

    /**
     * Get charset content from its name.
     */
    private function getCharsetContent(string $charsetName): string
    {
        switch ($charsetName) {
            case 'numeric':
                return self::DIGIT_CHARSET;
            case 'alphabetic':
                return self::LOWER_CHARSET . self::UPPER_CHARSET;
            case 'lowercase':
                return self::LOWER_CHARSET;
            case 'uppercase':
                return self::UPPER_CHARSET;
            case 'alphanumeric':
                return self::DIGIT_CHARSET . self::LOWER_CHARSET . self::UPPER_CHARSET;
        }

        return ''; // no needed. Only for phpstan use.
    }

    /**
     * Generate random sentence from given charset and length
     */
    private function generateSentence(string $charset, int $length): string
    {
        $sentence = '';

        for ($i = 0; $i < $length; $i++) {
            $sentence .= substr(str_shuffle($charset), 0, 1);
        }

        return $sentence;
    }
}
