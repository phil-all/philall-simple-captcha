<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\DataProvider;

use PhilallSimpleCaptcha\Picture\Picture;

/**
 * Uri generator supply image uri data scheme
 */
class UriGenerator implements UriGeneratorInterface
{
    public function __construct(private Picture $picture)
    {
    }

    public function getDataUriScheme(string $sentence, int $size, string $mimeType, string $contrast = null): string
    {
        return 'data:' . $mimeType . ';base64,' . $this->picture->getImageCode($sentence, $size, $mimeType, $contrast);
    }
}
