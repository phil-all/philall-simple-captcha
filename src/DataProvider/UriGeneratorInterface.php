<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace  PhilallSimpleCaptcha\DataProvider;

/**
 * Uri generator supply uri datas
 */
interface UriGeneratorInterface
{
    public function getDataUriScheme(string $sentence, int $size, string $mimeType, string $contrast = null): string;
}
