<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

namespace PhilallSimpleCaptcha\DataProvider;

/**
 * Randomizer interface
 */
interface RandomizerInterface
{
    /**
     * Generate sentence from charset name and sentence length.
     */
    public function getSentence(string $charsetName, int $length): string;
}
