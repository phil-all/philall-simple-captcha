<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha;

use PhilallSimpleCaptcha\CaptchaBuilderInterface;
use PhilallSimpleCaptcha\DataProvider\RandomizerInterface;
use PhilallSimpleCaptcha\DataProvider\UriGeneratorInterface;
use PhilallSimpleCaptcha\DataStorage\CookieHandlerInterface;

/**
 * Captcha builder is used to return a data URI scheme containing an encoded
 * captcha image, and store an encrypted cookie on client.
 */
class CaptchaBuilder implements CaptchaBuilderInterface
{
    private const NUM      = 'numeric';
    private const ALPHA    = 'alphabetic';
    private const LOWER    = 'lowercase';
    private const UPPER    = 'uppercase';
    private const ALPHANUM = 'alphanumeric';

    public function __construct(
        private RandomizerInterface $random,
        private UriGeneratorInterface $uri,
        private CookieHandlerInterface $cookieHandler
    ) {
    }

    /**
     * @inheritDoc
     */
    public function getNumericLight(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getLightCaptcha(
            $this->random->getSentence(self::NUM, $length),
            $fontSize,
            $mimeType
        );
    }
    /**
     * @inheritDoc
     */
    public function getNumericDark(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getDarkCaptcha(
            $this->random->getSentence(self::NUM, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getAlphabeticLight(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getLightCaptcha(
            $this->random->getSentence(self::ALPHA, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getAlphabeticDark(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getDarkCaptcha(
            $this->random->getSentence(self::ALPHA, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getLowercaseLight(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getLightCaptcha(
            $this->random->getSentence(self::LOWER, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getLowercaseDark(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getDarkCaptcha(
            $this->random->getSentence(self::LOWER, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getUppercaseLight(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getLightCaptcha(
            $this->random->getSentence(self::UPPER, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getUppercaseDark(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getDarkCaptcha(
            $this->random->getSentence(self::UPPER, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getAlphaNumericLight(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getLightCaptcha(
            $this->random->getSentence(self::ALPHANUM, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * @inheritDoc
     */
    public function getAlphaNumericDark(int $length, int $fontSize, string $mimeType): string
    {
        return $this->getDarkCaptcha(
            $this->random->getSentence(self::ALPHANUM, $length),
            $fontSize,
            $mimeType
        );
    }

    /**
     * Process light captcha.
     */
    private function getLightCaptcha(string $sentence, int $fontSize, string $mimeType): string
    {
        $this->cookieHandler->createCookie($sentence);
        return $this->uri->getDataUriScheme($sentence, $fontSize, $mimeType);
    }

    /**
     * Process dark captcha.
     */
    private function getDarkCaptcha(string $sentence, int $fontSize, string $mimeType): string
    {
        $this->cookieHandler->createCookie($sentence);
        return $this->uri->getDataUriScheme($sentence, $fontSize, $mimeType, 'dark');
    }
}
