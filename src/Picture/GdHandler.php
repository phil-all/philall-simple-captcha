<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\Picture;

use GdImage as Gd;

/**
 * Handle GD Object
 */
class GdHandler
{
    private const DS = DIRECTORY_SEPARATOR;

    private const FONT_FOLDER = 'Font';

    private const FONT = 'MAROLA__.TTF';

    private const LOCAL_FONT_PATH = self::DS . self::FONT_FOLDER . self::DS . self::FONT;

    /**
     * Get a GdImage representing a given sentence.
     */
    public function getGdImage(string $sentence, int $size, ?string $contrast): Gd
    {
        $width    = strlen($sentence) * $size;
        $height   = $size + 6;
        $img      = imagecreatetruecolor($width, $height);
        $fontFile = dirname(__FILE__) . self::LOCAL_FONT_PATH;

        $bgColor  = imagecolorallocate($img, 0, 0, 0);
        $txtColor = imagecolorallocate($img, 255, 255, 255);

        if (null == $contrast) {
            $bgColor  = imagecolorallocate($img, 255, 255, 255);
            $txtColor = imagecolorallocate($img, 0, 0, 0);
        }

        imagefilledrectangle($img, 0, 0, $width, $height, $bgColor);

        imagettftext($img, $size, 0, 3, $height, $txtColor, $fontFile, $sentence);

        return $img;
    }
}
