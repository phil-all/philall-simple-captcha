<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\Picture;

/**
 * Picture is used to get base_64 encoded picture
 * from given sentence font size and mime type for
 * jpeg or png image.
 */
class Picture implements PictureInterface
{
    public function __construct(private GdHandler $gdImage, private BufferOutput $buffer)
    {
    }

    /**
     * @inheritDoc
     */
    public function getImageCode(string $sentence, int $size, string $mimeType, string $contrast = null): string
    {
        $img = $this->gdImage->getGdImage($sentence, $size, $contrast);

        $imageCode = $this->buffer->getImageCodeFromBuffer($img, $mimeType);

        return base64_encode($imageCode);
    }
}
