<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\Picture;

/**
 * Picture is used to transfrom a string in an encoded picture.
 */
interface PictureInterface
{
    /**
     * Gets the base_64 encoded jpeg or png picture, generated from a sentence,
     * fontsize, a mime type and a contrast mode.
     */
    public function getImageCode(string $sentence, int $size, string $mimeType, string $contrast): string;
}
