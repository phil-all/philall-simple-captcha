<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\Picture;

use GdImage;

/**
 * Output buffering class
 */
class BufferOutput
{
    /**
     * Output a given type image in a returned variable,
     * image is not saved and destroy after outputing.
     */
    public function getImageCodeFromBuffer(GdImage $img, string $mimeType): string
    {
        ob_start();

        $mimeType === 'image/jpeg' ? imagejpeg($img) : imagepng($img);

        $imageCode = ob_get_contents();

        ob_end_clean();

        imagedestroy($img);

        return $imageCode;
    }
}
