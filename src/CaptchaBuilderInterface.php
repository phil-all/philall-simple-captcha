<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

namespace PhilallSimpleCaptcha;

/**
 * Captcha builder interface
 */
interface CaptchaBuilderInterface
{
    /**
     * Build only digits 0-9 captcha
     */
    public function getNumericLight(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build only digits 0-9 captcha
     */
    public function getNumericDark(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build only alphabetics a-ZA-Z captcha
     */
    public function getAlphabeticLight(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build only alphabetics a-ZA-Z captcha
     */
    public function getAlphabeticDark(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build only lowercase alphabetics a-z captcha
     */
    public function getLowercaseLight(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build only lowercase alphabetics a-z captcha
     */
    public function getLowercaseDark(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build only uppercase alphabetics A-Z captcha
     */
    public function getUppercaseLight(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build only uppercase alphabetics A-Z captcha
     */
    public function getUppercaseDark(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build alphanumerics a-zA-Z0-9 captcha
     */
    public function getAlphaNumericLight(int $length, int $fontSize, string $mimeType): string;

    /**
     * Build alphanumerics a-zA-Z0-9 captcha
     */
    public function getAlphaNumericDark(int $length, int $fontSize, string $mimeType): string;
}
