<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\DataTransformer;

/**
 * Sentence encryption and verification.
 */
class Encoder implements EncoderInterface
{
    public function getEncrypt(string $sentence): string
    {
        return password_hash(strrev($sentence), PASSWORD_BCRYPT, [
            'cost' => 10,
        ]);
    }
}
