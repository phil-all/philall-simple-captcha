<?php

/**
 * This file is part of the philall-simple-captcha.
 *
 * PHP version 8.1
 *
 * @author   Philippe Allard-Latour <phil.all.dev@gmail.com>
 * @license  https://opensource.org/licenses/MIT MIT
 * @link     https://gitlab.com/phil-all/philall-simple-captcha
 * @since    File available since Release 2.0 Beta
 *
 * This package is Open Source.
 */

declare(strict_types=1);

namespace PhilallSimpleCaptcha\DataStorage;

use PhilallSimpleCaptcha\DataTransformer\EncoderInterface;

/**
 *  * Cookie handler is used to store and read cookie from philall-simple-captcha.
 */
class CookieHandler implements CookieHandlerInterface
{
    private const COOKIE_NAME = "philall_simple_captcha";

    public function __construct(private EncoderInterface $encoder)
    {
    }

    public function createCookie(string $sentence): void
    {
        setcookie(self::COOKIE_NAME, $this->encoder->getEncrypt($sentence));
    }
}
