# Structure

```bash
# in application src/Service folder

PhilAll-SimpleCaptcha

 ├─DataProvider
 │  ├─Randomizer.php
 │  ├─RandomizerInterface.php
 │  ├─UriGenerator.php
 │  └─UriGeneratorInterface.php
 ├─DataStorage
 │  ├─CookieHandler.php
 │  └─CookieHandlerInterface.php
 ├─DataTransformer
 │  ├─Encoder.php
 │  └─EncoderInterface.php
 ├─Picture
 │  ├─Font
 │  │  └─MAROLA__.TTF  # credits to Nú-Dës designer, see http://www.nudes.com.br/
 │  ├─BufferOutput.php
 │  ├─GdHandler.php
 │  ├─Picture.php
 │  └─PictureInterface.php
 ├─CaptchaBuilder.php
 └─CaptchaValidator.php
```

**CaptchaBuilder**
Provide captcha embedded picture URI data scheme.
Set a captcha cookie.

**CaptchaValidator**
Checks user input validity.

**Randomizer**
Generate a random phrase.

**UriGenerator**
Provide an embedded picture URI data scheme.

**Picture**
Generate image.
Supply base 64 image code.

**GdHandler**
Handle GD Object.

**BufferOutput**
Output buffering class.

**CookieHandler**
Set captcha cookie from generated phrase.
Get captcha cookie.

**Encoder**
Sentence encryption and verification.

Return to [previous page](../README.md)
