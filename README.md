# philall-simple-captcha

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/15b1428d57a3490896ccf43ee35982cd)](https://www.codacy.com/gl/phil-all/philall-simple-captcha/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=phil-all/philall-simple-captcha&utm_campaign=Badge_Grade)

## 🚧 Version 2.0 / Under development...

* * *

[link to structure](./doc/structure.md).

[link to sequence diagram](./doc/sequence_diagram.md)

* * *

## Requirements

PHP &lt;= 8.0, with gd extension installed and enabled.

## How use it ?

### Generate and display captcha

Build and generate captcha in your backend :

```php
use PhilallSimpleCaptcha\CaptchaBuidlerInterface

// ...

// your class constructor
public function __construct(CaptchaBuidlerInterface $captcha)
{
}

//...

// in your code
$length   = ... // int, your captcha string length
$size     = ... // int, your captcha font size
$mimeType = ... // 'image/jpeg' or 'image/png'


// You can obtain different captcha charset type, and contrast as follow :

// Light contrast captchas
//
// to obtain only digits 0-9
$dataScheme = $captcha->getNumericLight($length, $size, $mimeType);

// to obtain only alphabetics a-ZA-Z
$dataScheme = $captcha->getAlphabeticLight($length, $size, $mimeType);

// to obtain only lowercase alphabetics a-z
$dataScheme = $captcha->getLowercaseLight($length, $size, $mimeType);

// to obtain only uppercase alphabetics A-Z
$dataScheme = $captcha->getUppercaseLight($length, $size, $mimeType);

// to obtain alphanumerics a-zA-Z0-9
$dataScheme = $captcha->getAlphaNumericLight($length, $size, $mimeType);

// Dark contrast captchas
//
// to obtain only digits 0-9
$dataScheme = $captcha->getNumericDark($length, $size, $mimeType);

// to obtain only alphabetics a-ZA-Z
$dataScheme = $captcha->getAlphabeticDark($length, $size, $mimeType);

// to obtain only lowercase alphabetics a-z
$dataScheme = $captcha->getLowercaseDark($length, $size, $mimeType);

// to obtain only uppercase alphabetics A-Z
$dataScheme = $captcha->getUppercaseDark($length, $size, $mimeType);

// to obtain alphanumerics a-zA-Z0-9
$dataScheme = $captcha->getAlphaNumericDark($length, $size, $mimeType);

```

Exemple in your frontend :

```html
<!-- PHP 'vanilla' project -->
<img src=<?= $dataScheme ?> alt="Captcha" />

<!-- Symfony project -->
<img src={{ dataScheme }} alt="Captcha" />
```

### User input validation

... ASAP
